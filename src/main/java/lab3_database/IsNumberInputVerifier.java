package lab3_database;

import org.apache.commons.lang3.math.NumberUtils;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class IsNumberInputVerifier extends InputVerifier {
    private Border defultborder = null;
    @Override
    public boolean verify(JComponent input) {
        if ( !(input instanceof JTextField) )
            return false;
        if (defultborder == null)
            defultborder = input.getBorder();

        String text = ((JTextField) input).getText();
        if (NumberUtils.isNumber(text) || text.equals("")){
            input.setBorder(defultborder);
            return true;
        }
        input.setBorder(BorderFactory.createLineBorder(Color.RED));
        return false;
    }
}
