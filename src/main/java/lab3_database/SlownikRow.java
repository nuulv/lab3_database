package lab3_database;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class SlownikRow {
    private Integer gr;
    private Integer podGr;
    private Integer rodz;
    private String typ;
    private String opis;
    private Integer nrOdpadu;
}
