package lab3_database;

import org.apache.commons.lang3.math.NumberUtils;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.util.Scanner;

public class IsCardNumberInputVerifier extends InputVerifier {
    private Border defultborder = null;
    @Override
    public boolean verify(JComponent input) {
        if ( !(input instanceof JTextField) )
            return false;
        if (defultborder == null)
            defultborder = input.getBorder();

        String text = ((JTextField) input).getText();

        Scanner scanner = new Scanner(text);
        scanner.useDelimiter("/");

        int i = 0;
        while(scanner.hasNext()){
            if (NumberUtils.isNumber(scanner.next())){
                i++;
            }
        }

        if (i == 3){
            input.setBorder(defultborder);
            return true;
        }


        input.setBorder(BorderFactory.createLineBorder(Color.RED));
        return false;
    }
}
