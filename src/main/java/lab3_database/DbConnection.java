package lab3_database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_HOST = "localhost";
    static final String DB_PORT = "3306";
    static final String DB_NAME = "database";

    static final String USER = "root";
    static final String PASS = "";

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        return getConnection(DB_HOST, DB_PORT, DB_NAME, USER, PASS);
    }

    public static Connection getConnection(String hostName, String port, String sid, String userName, String password)
            throws ClassNotFoundException, SQLException {

        try {
            Class.forName(JDBC_DRIVER).newInstance();
        } catch (Exception except) {
            except.printStackTrace();
        }
        String connectionURL = "jdbc:mysql://" + hostName + ":" + port + "/" + sid;

        Connection conn = DriverManager.getConnection(connectionURL, userName, password);
        return conn;
    }
}
