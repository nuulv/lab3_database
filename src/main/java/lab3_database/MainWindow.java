package lab3_database;

import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@Slf4j
public class MainWindow {
    private JPanel panel1;
    private JTable table1;
    private JPanel panel2;
    private JPanel panel3;

    private JMenuBar menuBar;

    private JPopupMenu popupMenu;

    private Connection conn;

    private String selectedTable = "";


    public static void main(String[] args) {
        JFrame frame = new JFrame("MainWindow");
        frame.setContentPane(new MainWindow().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public MainWindow() {
        initMenu();
        initPopupMenu();
        initTable();
        try {
            this.conn = DbConnection.getConnection();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void initMenu() {
        this.menuBar = new JMenuBar();

        //create menus
        JMenu fileMenu = new JMenu("File");

        JMenuItem open1MenuItem = new JMenuItem("Load data from magazynp");
        open1MenuItem.setActionCommand("load-magazynp");
        JMenuItem open2MenuItem = new JMenuItem("Load data from slownik");
        open2MenuItem.setActionCommand("load-slownik");
        // actionListener
        open1MenuItem.addActionListener(e -> menuListener(e));
        open2MenuItem.addActionListener(e -> menuListener(e));

        //add menu items to menus
        fileMenu.add(open1MenuItem);
        fileMenu.add(open2MenuItem);
        //add menu to menubar
        menuBar.add(fileMenu);
        // add menu to panel
        panel2.add(menuBar, BorderLayout.NORTH);
    }

    private void initPopupMenu() {
        popupMenu = new JPopupMenu();
        JMenuItem i = new JMenuItem("insert");
        i.setActionCommand("insert");
        i.addActionListener(event -> popupMenuListener(event));
        JMenuItem e = new JMenuItem("edit");
        e.setActionCommand("edit");
        e.addActionListener(event -> popupMenuListener(event));
        JMenuItem d = new JMenuItem("delete");
        d.setActionCommand("delete");
        d.addActionListener(event -> popupMenuListener(event));

        popupMenu.add(i);
        popupMenu.add(e);
        popupMenu.add(d);
    }

    private void initTable() {
        table1.setComponentPopupMenu(popupMenu);
    }

    private void menuListener(ActionEvent event) {
        switch (event.getActionCommand()) {
            case "load-magazynp":
                try {
                    String query = "select * from magazynp";
                    ResultSet rs;
                    PreparedStatement stmt;
                    stmt = conn.prepareStatement(query);
                    rs = stmt.executeQuery();

                    ResultSetMetaData metaData = rs.getMetaData();

                    List<MagazynpRow> lista = new ArrayList<>();

                    // map resultSet to list of objects
                    while (rs.next()) {
                        MagazynpRow obj = new MagazynpRow(
                                (Integer) rs.getObject(1),
                                new CardNumber((String) rs.getObject(2)),
                                (Integer) rs.getObject(3),
                                (Integer) rs.getObject(4),
                                (Integer) rs.getObject(5),
                                (String) rs.getObject(6),
                                (Double) rs.getObject(7),
                                (java.sql.Date) rs.getObject(8)
                        );
                        lista.add(obj);

                        //TableModel<MagazynpRow> tableModel = new TableModel<>(lista);

                        table1.setModel(new TableModel(lista));
                        selectedTable = "magazynp";
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case "load-slownik":
                try {
                    String query = "select * from slownik";
                    ResultSet rs;
                    PreparedStatement stmt;
                    stmt = conn.prepareStatement(query);
                    rs = stmt.executeQuery();

                    ResultSetMetaData metaData = rs.getMetaData();

                    List<SlownikRow> lista = new ArrayList<>();

                    // map resultSet to list of objects
                    while (rs.next()) {
                        SlownikRow obj = new SlownikRow(
                                (Integer) rs.getObject("gr"),
                                (Integer) rs.getObject("podgr"),
                                (Integer) rs.getObject("rodz"),
                                (String) rs.getObject("typ"),
                                (String) rs.getObject("opis"),
                                (Integer) rs.getObject("nr_odpadu")
                        );
                        lista.add(obj);

                        table1.setModel(new TableModel(lista));
                        selectedTable = "slownik";
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }

    }

    private void popupMenuListener(ActionEvent event) {
        int selectedRow = table1.getSelectedRow();
        TableModel m = (TableModel) table1.getModel();
        switch (event.getActionCommand()) {
            case "delete":
                if (selectedRow != -1) {
                    // usuń wiersz
                    try {
                        String query;
                        if (selectedTable.equals("magazynp")) {
                            query = "DELETE FROM magazynp WHERE  nr_mag = ?";
                        } else {
                            query = "DELETE FROM slownik WHERE  nr_odpadu = ?";
                        }
                        PreparedStatement stmt;
                        stmt = conn.prepareStatement(query);

                        if (selectedTable.equals("magazynp")) {
                            MagazynpRow row = (MagazynpRow) m.getRowAt(selectedRow);
                            stmt.setInt(1, row.getNrMag());
                        } else {
                            SlownikRow row = (SlownikRow) m.getRowAt(selectedRow);
                            stmt.setInt(1, row.getNrOdpadu());
                        }
                        stmt.executeUpdate();
                        m.removeRow(selectedRow);
                        table1.repaint();
                    } catch (SQLException e) {
                        JOptionPane.showMessageDialog(panel1, e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
                    }
                }
                break;
            default:
                if (selectedTable.equals("magazynp")){
                    InsertUpdateMagazynp dialog;
                    if (selectedRow != -1 && event.getActionCommand().equals("edit")) {
                        dialog = new InsertUpdateMagazynp((MagazynpRow) m.getRowAt(selectedRow));
                    } else {
                        dialog = new InsertUpdateMagazynp();
                    }
                    dialog.pack();
                    dialog.setVisible(true);

                    if (dialog.status) {
                        insertUpdateMagazynp(dialog.getData());
                        if (event.getActionCommand().equals("insert")){
                            List<MagazynpRow> row = new ArrayList<>();
                            row.add(0, dialog.getData());

                            m.addRows(row);
                        }
                        if (event.getActionCommand().equals("edit")){
                            m.updateRow(selectedRow, dialog.getData());
                        }
                    }
                }else{
                    InsertUpdateSlownik dialog;
                    if (selectedRow != -1 && event.getActionCommand().equals("edit")) {
                        dialog = new InsertUpdateSlownik((SlownikRow) m.getRowAt(selectedRow));
                    }else {
                        dialog = new InsertUpdateSlownik();
                    }
                    dialog.pack();
                    dialog.setVisible(true);

                    if (dialog.status) {
                        insertUpdateSlownik(dialog.getData());
                        if (event.getActionCommand().equals("insert")){
                            List<SlownikRow> row = new ArrayList<>();
                            row.add(0, dialog.getData());

                            m.addRows(row);
                        }
                        if (event.getActionCommand().equals("edit")){
                            m.updateRow(selectedRow, dialog.getData());
                        }
                    }
                }
                table1.repaint();
                break;
        }
    }

    private void insertUpdateMagazynp(MagazynpRow data) {
        try {
            String call = "{call insertUpdateMagazynp(?, ?, ?, ?, ?, ?, ? ,?)}";
            CallableStatement cStmt = conn.prepareCall(call);
            cStmt.setInt(1, data.getNrMag());
            cStmt.setString(2, data.getNrKarty().toString());
            cStmt.setInt(3, data.getNrOdpadu());
            cStmt.setInt(4, data.getNrKlienta());
            cStmt.setInt(5, data.getFirma());
            cStmt.setString(6, data.getJednostka());
            cStmt.setDouble(7, data.getMasa());
            cStmt.setDate(8, data.getDataD());

            cStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void insertUpdateSlownik(SlownikRow data) {
        try {
            String call = "{call insertUpdateSlownik(?, ?, ?, ?, ?, ?)}";
            CallableStatement cStmt = conn.prepareCall(call);
            cStmt.setInt(1, data.getNrOdpadu());
            cStmt.setInt(2, data.getGr());
            cStmt.setInt(3, data.getPodGr());
            cStmt.setInt(4, data.getRodz());
            cStmt.setString(5, data.getTyp());
            cStmt.setString(6, data.getOpis());

            cStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}