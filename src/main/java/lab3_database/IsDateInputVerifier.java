package lab3_database;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class IsDateInputVerifier extends InputVerifier {
    private Border defultborder = null;
    @Override
    public boolean verify(JComponent input) {
        if ( !(input instanceof JTextField) )
            return false;
        if (defultborder == null)
            defultborder = input.getBorder();

        String text = ((JTextField) input).getText();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(false);

        try {
            //if not valid, it will throw ParseException
            Date date = sdf.parse(text);
            input.setBorder(defultborder);
            return true;

        } catch (ParseException e) {
            input.setBorder(BorderFactory.createLineBorder(Color.RED));
            return false;
        }
    }
}
