package lab3_database;

import javax.swing.*;
import java.awt.event.*;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class InsertUpdateMagazynp extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField nr_magazynuField;
    private JTextField nr_kartyField;
    private JTextField nr_odpaduField;
    private JTextField nr_klientaField;
    private JTextField firmaField;
    private JTextField jednField;
    private JTextField masaField;
    private JTextField dataField;

    public boolean status = true;

    private MagazynpRow currentRow = new MagazynpRow();

    public InsertUpdateMagazynp() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        currentRow = new MagazynpRow();

        nr_magazynuField.setInputVerifier(new IsNumberInputVerifier());
        nr_kartyField.setInputVerifier(new IsCardNumberInputVerifier());
        nr_odpaduField.setInputVerifier(new IsNumberInputVerifier());
        nr_klientaField.setInputVerifier(new IsNumberInputVerifier());
        firmaField.setInputVerifier(new IsNumberInputVerifier());
        masaField.setInputVerifier(new IsNumberInputVerifier());
        dataField.setInputVerifier(new IsDateInputVerifier());
    }

    public InsertUpdateMagazynp(MagazynpRow row){
        this(); // run default constructor

        currentRow = row;

        nr_magazynuField.setText(row.getNrMag().toString());
        nr_magazynuField.setEnabled(false);
        nr_kartyField.setText(row.getNrKarty().toString());
        nr_odpaduField.setText(row.getNrOdpadu().toString());
        nr_klientaField.setText(row.getNrKlienta().toString());
        firmaField.setText(row.getFirma().toString());
        jednField.setText(row.getJednostka());
        masaField.setText(row.getMasa().toString());
        dataField.setText(row.getDataD().toString());
    }

    private void onOK() {
        // add your code here

        currentRow.setNrMag(Integer.valueOf(nr_magazynuField.getText()));
        currentRow.setNrKarty(new CardNumber(nr_kartyField.getText()));
        currentRow.setNrOdpadu(Integer.valueOf(nr_odpaduField.getText()));
        currentRow.setNrKlienta(Integer.valueOf(nr_klientaField.getText()));
        currentRow.setFirma(Integer.valueOf(firmaField.getText()));
        currentRow.setJednostka(jednField.getText());
        currentRow.setMasa(Double.valueOf(masaField.getText()));
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = null;
        try {
            date = format.parse(dataField.getText());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        currentRow.setDataD(new Date(date.getTime()));

        status = true;
        setVisible(false);
        //dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        status = false;
        dispose();
    }

    public MagazynpRow getData(){
        return currentRow;
    }

    public static void main(String[] args) {
        InsertUpdateMagazynp dialog = new InsertUpdateMagazynp();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
