package lab3_database;

import lombok.*;

import java.io.Serializable;
import java.sql.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class MagazynpRow implements Serializable{
    private Integer nrMag;
    private CardNumber nrKarty;
    private Integer nrOdpadu;
    private Integer nrKlienta;
    private Integer firma;
    private String jednostka;
    private Double masa;
    private Date dataD;
}