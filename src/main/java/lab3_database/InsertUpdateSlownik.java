package lab3_database;

import javax.swing.*;
import java.awt.event.*;

public class InsertUpdateSlownik extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField nr_odpaduField;
    private JTextField grupaField;
    private JTextField podgrField;
    private JTextField typField;
    private JTextField rodzField;
    private JTextField opisField;

    private SlownikRow currentRow = new SlownikRow();

    public boolean status = true;

    public InsertUpdateSlownik() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        nr_odpaduField.setInputVerifier(new IsNumberInputVerifier());
        grupaField.setInputVerifier(new IsNumberInputVerifier());
        podgrField.setInputVerifier(new IsNumberInputVerifier());
        rodzField.setInputVerifier(new IsNumberInputVerifier());
    }

    public InsertUpdateSlownik(SlownikRow row){
        this();

        currentRow = row;

        nr_odpaduField.setText(row.getNrOdpadu().toString());
        nr_odpaduField.setEnabled(false);
        grupaField.setText(row.getGr().toString());
        podgrField.setText(row.getPodGr().toString());
        typField.setText(row.getTyp().toString());
        rodzField.setText(row.getRodz().toString());
        opisField.setText(row.getOpis());
    }

    private void onOK() {
        // add your code here
        currentRow.setNrOdpadu(Integer.valueOf(nr_odpaduField.getText()));
        currentRow.setGr(Integer.valueOf(grupaField.getText()));
        currentRow.setPodGr(Integer.valueOf(podgrField.getText()));
        currentRow.setTyp(typField.getText());
        currentRow.setRodz(Integer.valueOf(rodzField.getText()));
        currentRow.setOpis(opisField.getText());

        status = true;
        setVisible(false);
        //dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        status = false;
        dispose();
    }

    public SlownikRow getData(){
        return currentRow;
    }

    public static void main(String[] args) {
        InsertUpdateSlownik dialog = new InsertUpdateSlownik();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
