package importData;

import com.opencsv.CSVReader;
import lab3_database.DbConnection;
import lab3_database.SlownikRow;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class InsertSlownikToDatabase {

    public static void main(String[] args) {

        //read csv

        String path = "Slownik.csv";


        CSVReader reader = null;
        List<SlownikRow> rows = new ArrayList<>();
        //! try to open file and read data
        try {
            //Build reader instance
            reader = new CSVReader(new FileReader(path), ';');
            List<String[]> records = reader.readAll();
            Iterator<String[]> iterator = records.iterator();

            //skip header row
            String[] header = iterator.next();

            while (iterator.hasNext()) {
                String[] record = iterator.next();
                SlownikRow item = new SlownikRow(
                        NumberUtils.toInt(record[0].replaceAll("\\D+", "")),
                        NumberUtils.toInt(record[1].replaceAll("\\D+", "")),
                        NumberUtils.toInt(record[2].replaceAll("\\D+", "")),
                        record[3],
                        record[4],
                        NumberUtils.toInt(record[5].replaceAll("\\D+", ""))
                );
                rows.add(item);
            }
        } catch (Exception ex) {
            //logger.error("Error! file", ex);
            ex.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException | NullPointerException ex) {
                //logger.error("Reader close error!", ex);
                ex.printStackTrace();
            }
        }


        // insert csv to database
        try (Connection conn = DbConnection.getConnection()){

            PreparedStatement stmt;
            conn.setAutoCommit(false);

            String query = "insert into slownik(nr_odpadu, gr, podgr, rodz, typ, opis) " +
                    "values(?, ?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(query);
            for (SlownikRow row : rows) {
                stmt.setInt(1, row.getNrOdpadu());
                stmt.setInt(2, row.getGr());
                stmt.setInt(3, row.getPodGr());
                stmt.setInt(4, row.getRodz());
                stmt.setString(5, row.getTyp());
                stmt.setString(6, row.getOpis());
                stmt.addBatch();
            }
            stmt.executeBatch();
            conn.commit();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
