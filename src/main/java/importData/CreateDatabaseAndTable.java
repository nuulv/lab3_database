package importData;

import lab3_database.DbConnection;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.SQLException;

public class CreateDatabaseAndTable {
    public static void main(String[] args) {
        try (Connection conn = DbConnection.getConnection()){

            Statement stmt;
            String query;
            conn.setAutoCommit(false);

            /// Create database
//            String query = "CREATE DATABASE `database`";
//            stmt = conn.prepareStatement(query);
//            stmt.execute();
//            conn.commit();


            /// Create tables
            query = "CREATE TABLE `slownik` (\n" +
                    "\t`nr_odpadu` INT(11) NOT NULL AUTO_INCREMENT,\n" +
                    "\t`gr` INT(11) NULL DEFAULT NULL,\n" +
                    "\t`podgr` INT(11) NULL DEFAULT NULL,\n" +
                    "\t`rodz` INT(11) NULL DEFAULT NULL,\n" +
                    "\t`typ` VARCHAR(50) NULL DEFAULT NULL,\n" +
                    "\t`opis` TEXT NULL,\n" +
                    "\tPRIMARY KEY (`nr_odpadu`)\n" +
                    ")\n" +
                    "ENGINE=InnoDB\n" +
                    ";";
            stmt = conn.createStatement();
            stmt.executeUpdate(query);


            query = "CREATE TABLE `magazynp` (\n" +
                    "\t`nr_mag` INT(11) NOT NULL AUTO_INCREMENT,\n" +
                    "\t`nr_karty` VARCHAR(50) NULL DEFAULT NULL,\n" +
                    "\t`nr_odpadu` INT(11) NULL DEFAULT NULL,\n" +
                    "\t`nr_klienta` INT(11) NULL DEFAULT NULL,\n" +
                    "\t`firma` INT(11) NULL DEFAULT NULL,\n" +
                    "\t`jedn` VARCHAR(50) NULL DEFAULT NULL,\n" +
                    "\t`masa` DOUBLE NULL DEFAULT NULL,\n" +
                    "\t`datad` DATE NULL DEFAULT NULL,\n" +
                    "\tPRIMARY KEY (`nr_mag`),\n" +
                    "\tINDEX `FK__slownik` (`nr_odpadu`),\n" +
                    "\tCONSTRAINT `FK__slownik` FOREIGN KEY (`nr_odpadu`) REFERENCES `slownik` (`nr_odpadu`)\n" +
                    ")\n" +
                    "ENGINE=InnoDB\n" +
                    ";\n";
            stmt = conn.createStatement();
            stmt.executeUpdate(query);

            // Commit changes
            conn.commit();

        }catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
