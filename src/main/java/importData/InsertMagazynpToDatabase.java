package importData;


import com.opencsv.CSVReader;
import lab3_database.CardNumber;
import lab3_database.DbConnection;
import lab3_database.MagazynpRow;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class InsertMagazynpToDatabase {

    public static void main(String[] args) {

        //read csv

        String path = "Magazynp.csv";


        CSVReader reader = null;
        List<MagazynpRow> rows = new ArrayList<>();
        //! try to open file and read data
        try {
            //Build reader instance
            reader = new CSVReader(new FileReader(path), ';');
            List<String[]> records = reader.readAll();
            Iterator<String[]> iterator = records.iterator();

            //skip header row
            String[] header = iterator.next();

            while (iterator.hasNext()) {
                String[] record = iterator.next();
                DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                Date date = format.parse(record[7]);
                MagazynpRow item = new MagazynpRow(
                        NumberUtils.toInt(record[0].replaceAll("\\D+", "")),
                        new CardNumber(record[1]),
                        NumberUtils.toInt(record[2].replaceAll("\\D+", "")),
                        NumberUtils.toInt(record[3].replaceAll("\\D+", "")),
                        NumberUtils.toInt(record[4].replaceAll("\\D+", "")),
                        record[5],
                        NumberUtils.toDouble(record[6]),
                        new java.sql.Date(date.getTime())
                );
                rows.add(item);
            }
        } catch (Exception ex) {
            //logger.error("Error! file", ex);
            ex.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException | NullPointerException ex) {
                //logger.error("Reader close error!", ex);
                ex.printStackTrace();
            }
        }




        // insert csv to database
        try (Connection conn = DbConnection.getConnection()){

            PreparedStatement stmt;
            conn.setAutoCommit(false);

            String query = "insert into magazynp(nr_mag, nr_karty, nr_odpadu, nr_klienta, firma, jedn, masa, datad) " +
                    "values(?, ?, ?, ?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(query);
            for (MagazynpRow row : rows) {
                stmt.setInt(1, row.getNrMag());
                stmt.setString(2, row.getNrKarty().toString());
                stmt.setInt(3, row.getNrOdpadu());
                stmt.setInt(4, row.getNrKlienta());
                stmt.setInt(5, row.getFirma());
                stmt.setString(6, row.getJednostka());
                stmt.setDouble(7, row.getMasa());
                stmt.setDate(8, row.getDataD());
                stmt.addBatch();
            }
            stmt.executeBatch();
            conn.commit();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
